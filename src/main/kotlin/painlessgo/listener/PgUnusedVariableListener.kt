package painlessgo.listener

import com.goide.GoFileType
import com.intellij.ide.actionsOnSave.impl.ActionsOnSaveFileDocumentManagerListener.ActionOnSave
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.editor.Document
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiManager
import painlessgo.psi.PgUnusedVariableProcessor

class PgUnusedVariableListener : ActionOnSave() {

    override fun isEnabledForProject(project: Project): Boolean {
        return true;
    }

    override fun processDocuments(project: Project, documents: Array<out Document>) {
        WriteCommandAction.runWriteCommandAction(project) {
            documents.forEach {
                processDocument(it, project)
            }
        }
    }

    fun processDocument(document: Document, project: Project) {
        val virtualFile = FileDocumentManager.getInstance().getFile(document) ?: return;

        val psiFile = PsiManager.getInstance(project).findFile(virtualFile) ?: return;
        if (psiFile.fileType !is GoFileType) return;

        PgUnusedVariableProcessor.processDocument(document, psiFile, project);
    }

}
