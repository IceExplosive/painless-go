package painlessgo.psi

import com.goide.psi.GoAssignmentStatement
import com.goide.psi.GoReferencesSearch
import com.goide.psi.GoVarDefinition
import com.goide.psi.impl.GoReferenceExpressionImpl
import com.intellij.codeInsight.actions.ReformatCodeProcessor
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.EditorModificationUtil
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiFile
import com.intellij.psi.search.LocalSearchScope
import com.intellij.psi.util.PsiEditorUtil
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset
import com.intellij.util.DocumentUtil
import painlessgo.psi.util.DocumentCorrections

object PgUnusedVariableProcessor {

    fun processDocument(document: Document, psiFile: PsiFile, project: Project) {
        val declarations = PsiTreeUtil.findChildrenOfType(psiFile, GoVarDefinition::class.java);
        val assignments = PsiTreeUtil.findChildrenOfType(psiFile, GoAssignmentStatement::class.java);
        val editor =
            PsiEditorUtil.findEditor(declarations.firstOrNull() ?: assignments.firstOrNull() ?: return) ?: return;
        val caret = editor.caretModel;
        val baseOffset = caret.offset;
        val corrections = DocumentCorrections();

        declarations.forEach declarations@{
            if (it.useScope !is LocalSearchScope) return@declarations;
            val references = GoReferencesSearch.search(it, it.useScope).findAll();
            val name = it.name ?: return@declarations;
            if (name == "_") return@declarations;

            when (references.size) {
                0 -> {
                    val usageLine = "_ = $name\n";
                    val offset = corrections.offset(it);
                    val endOffset = it.parent.endOffset;
                    val newLineIndex = DocumentUtil.getLineEndOffset(endOffset + offset, document)
                    editor.caretModel.moveToOffset(newLineIndex + 1);
                    EditorModificationUtil.insertStringAtCaret(editor, usageLine);
                    corrections.add(endOffset, usageLine.length);
                }

                1 -> {}
                else -> {
                    val expected = "_ = $name";
                    references.forEach {
                        val parent = it.element.parent
                        if (parent.text == expected) {
                            val offset = corrections.offset(parent);
                            val range = DocumentUtil.getLineTextRange(document, document.getLineNumber(parent.startOffset + offset));
                            document.deleteString(range.startOffset + offset, range.endOffset + offset + 1);
                            corrections.removedLine(range, offset);
                        }
                    }
                }
            }
        }

        assignments.forEach assignments@{
            val text = it.text;
            if (!text.startsWith("_ = ")) return@assignments;
            if (it.expressionList[0] !is GoReferenceExpressionImpl) return@assignments;

            val expression = it.expressionList[0] as GoReferenceExpressionImpl;
            val resolve = expression.resolve();
            if (resolve == null) {
                val offset = corrections.offset(it);
                val range = DocumentUtil.getLineTextRange(document, document.getLineNumber(it.startOffset + offset));
                document.deleteString(range.startOffset, range.endOffset + 1);
                corrections.removedLine(range, offset);
            }
        }

        editor.caretModel.moveToOffset(baseOffset + corrections.preOffset(baseOffset));
        PsiDocumentManager.getInstance(project).commitDocument(document);
        ReformatCodeProcessor(psiFile, false).run();
    }

}
