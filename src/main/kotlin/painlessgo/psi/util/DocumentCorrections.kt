package painlessgo.psi.util

import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.refactoring.suggested.startOffset

class DocumentCorrections {

    val offsets = hashMapOf<Int, Int>()

    fun removedLine(line: TextRange, offset: Int) {
        add(line.startOffset - offset, -line.length - 1);
    }

    fun add(offset: Int, range: Int) {
        offsets.put(offset, range);
    }

    fun offset(element: PsiElement): Int {
        return offset(element.startOffset);
    }

    fun offset(line: TextRange): Int {
        return offset(line.startOffset);
    }

    fun preOffset(offset: Int): Int {
        var correction = 0;
        offsets.forEach { at, range ->
            if (at > offset) correction += range;
        }

        return correction;
    }

    fun offset(offset: Int): Int {
        var correction = 0;
        offsets.forEach { at, range ->
            if (at < offset) correction += range;
        }

        return correction;
    }

    fun total(): Int {
        if (offsets.isEmpty()) return 0;

        return offsets.values.reduce { acc, i ->
            return acc + i
        }
    }

}
