package painlessgo.action.quickFix

import com.goide.psi.GoSimpleStatement
import com.intellij.codeInsight.intention.impl.BaseIntentionAction
import com.intellij.codeInsight.template.Template
import com.intellij.codeInsight.template.TemplateManager
import com.intellij.codeInsight.template.impl.TemplateSettings
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile
import com.intellij.refactoring.suggested.endOffset
import com.intellij.util.DocumentUtil

class PgHandleError(
        private val element: GoSimpleStatement,
        private val prefix: String,
        private val message: String,
        private val suffix: String,
) : BaseIntentionAction() {

    override fun getText(): String {
        return "Handle error";
    }

    override fun getFamilyName(): String {
        return "Handle error";
    }

    override fun isAvailable(project: Project, editor: Editor?, file: PsiFile?): Boolean {
        return true;
    }

    override fun invoke(project: Project, editor: Editor?, file: PsiFile?) {
        if (editor == null || file == null) return;

        val lineEnd = DocumentUtil.getLineEndOffset(element.endOffset, editor.document);
        editor.caretModel.moveToOffset(lineEnd + 1);

        val properties = hashMapOf<String, String>();
        properties["PREFIX"] = prefix;
        properties["MESSAGE"] = message;
        properties["SUFFIX"] = suffix;

        val template = TemplateSettings.getInstance().getTemplate("handle", "Go") as Template;
        TemplateManager.getInstance(project).startTemplate(editor, template, true, properties, null);
    }

}
