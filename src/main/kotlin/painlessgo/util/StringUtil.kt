package painlessgo.util

import ai.grazie.utils.capitalize
import java.util.*

object StringUtil {

    fun String.toSpacedCase(capitalize: Boolean = false): String {
        return this
            .replace(Regex("([a-z0-9])([A-Z])"), "$1 $2")
            .replace(Regex("([a-zA-Z])([0-9])"), "$1 $2")
            .replace(Regex("([A-Z])([A-Z])(?=[a-z])"), "$1 $2")
            .split("-", "_", " ")
            .joinToString(" ") {
                if (capitalize) it.capitalize() else it.lowercase(Locale.getDefault())
            }
    }

}
