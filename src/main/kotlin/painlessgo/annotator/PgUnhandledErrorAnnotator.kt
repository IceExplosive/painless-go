package painlessgo.annotator

import com.goide.psi.*
import com.goide.psi.impl.GoTypeUtil
import com.goide.util.GoZeroValue
import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.psi.PsiElement
import com.intellij.psi.ResolveState
import com.intellij.psi.search.LocalSearchScope
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.refactoring.suggested.startOffset
import com.intellij.util.ObjectUtils
import painlessgo.action.quickFix.PgHandleError
import painlessgo.util.StringUtil.toSpacedCase

class PgUnhandledErrorAnnotator : Annotator {

    override fun annotate(element: PsiElement, holder: AnnotationHolder) {
        if (element !is GoSimpleStatement) return;

        val definitions = PsiTreeUtil.findChildrenOfType(element, GoVarDefinition::class.java).toTypedArray();
        var errorIndex = definitions.indexOfFirst { GoTypeUtil.isError(it.getGoType(ResolveState()), element) };
        if (errorIndex < 0) return;

        val error = definitions[errorIndex];
        if (error.useScope !is LocalSearchScope) return;

        val references = GoReferencesSearch.search(error, error.useScope).findAll();
        if (
                references.any {
                    it.absoluteRange.startOffset > element.startOffset && it.element.parent.text != "_ = ${error.name}"
                }
        ) return;

        val functionContext = PsiTreeUtil.findFirstParent(element) { it is GoFunctionDeclaration } ?: return;
        val resultTypes = (functionContext as GoFunctionDeclaration).signature?.resultType ?: return;

        val types: List<GoType> = if (resultTypes is GoTypeList) {
            resultTypes.typeList;
        } else {
            listOf(resultTypes);
        }
        errorIndex = types.indexOfFirst { GoTypeUtil.isError(it, element) };

        val prefixes = mutableListOf<String>();
        val suffixes = mutableListOf<String>();
        var message = "";

        types.forEachIndexed { i, it ->
            if (i == errorIndex) {
                prefixes.add("fmt.Errorf(\"");
                message = functionContext.name.orEmpty().toSpacedCase();
                val name = error.name ?: "err";
                suffixes.add(": %w\", $name)");
            } else {
                val zeroValue = zeroValue(it);
                if (i < errorIndex) {
                    prefixes.add(zeroValue);
                } else {
                    suffixes.add(zeroValue);
                }
            }
        }

        holder
                .newAnnotation(HighlightSeverity.WEAK_WARNING, "Unprocessed error")
                .range(element.textRange)
                .withFix(
                        PgHandleError(
                                element,
                                prefixes.joinToString(", "),
                                message,
                                suffixes.joinToString(", "),
                        )
                )
                .create()
    }

    // Copy-pasted from GoReturnCompletionContributor
    private fun zeroValue(type: GoType): String {
        val zeroValue = GoZeroValue.of(type)
        return if (zeroValue == GoZeroValue.EMPTY_LITERAL) {
            val underlyingType: GoType = type.contextlessUnderlyingType
            if (underlyingType is GoStructType) {
                type.text + "{}"
            } else {
                if (GoTypeUtil.isArray(underlyingType, null as PsiElement?)) {
                    val arrayType = ObjectUtils.tryCast(
                            underlyingType,
                            GoArrayOrSliceType::class.java
                    )
                    if (arrayType != null) {
                        val elementType = arrayType.type
                        return String.format("[%d]%s{}", arrayType.length, elementType.text)
                    }
                }
                "nil"
            }
        } else {
            zeroValue.text!!
        }
    }

}
